<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Iniciar Sesion</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/customcss.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
	function cambiar(obj) {
		var selectBox = obj;
		var selected = selectBox.options[selectBox.selectedIndex].value;
		var divU = document.getElementById("user");
		var divC = document.getElementById("contacto");

		if (selected === 'C') {
			$('#contacto').show();
			$('#user').hide();

		} else {
			$('#contacto').hide();
			$('#user').show();
		}
	}
	$(window).load(function() {
		$('#myModal').modal('show');
		$('#contacto').show();
		$('#user').hide();
	});
</script>
</head>
<body>
		<div class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.jsp">CRUD</a>
			</div>
			<%
				capanegocio.Usuario usuario = (capanegocio.Usuario) session.getAttribute("usuario");
				if (usuario == null) {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="formularioCrearUsuario.jsp"><span
						class="glyphicon glyphicon-user"></span> Crear Usuario</a></li>
				<li class="active"><a href="formularioLogin.jsp"><span
						class="glyphicon glyphicon-log-in"></span> Log in</a></li>
			</ul>
			<%
				} else {
			%>
			<div id="navbar-main">
				<ul class="nav navbar-nav">
					<li><a href="formularioCrearContacto.jsp"><span
							class="glyphicon glyphicon-user"></span> Crear Contacto</a></li>
					<li>
						<form role="form" action="ServletPostGetContacto" method="get">
							<button type="submit" class="li-btn li-btn-primary">
								<span class="glyphicon glyphicon-list"></span> Lista de Contactos
							</button>
						</form>
					</li>
					<li>
						<form action="ServletPostGetUsuario" method="get">
							<button type="submit" class="li-btn li-btn-primary">
								<span class="glyphicon glyphicon-list"></span> Lista de
								Usuarios
							</button>
						</form>
					</li>
					<li><a href="buscadorAvanzado.jsp"><span
							class="glyphicon glyphicon-search"></span> Buscador Avanzado</a></li>
					<li>
				</ul>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><select id="tipoBusqueda" name="tipoBusqueda"
					onclick="cambiar(this)" class="form-control">
						<option value="C">Contacto</option>
						<option value="U">Usuario</option>
				</select></li>
				<li>
					<div id="contacto">
						<form method="post" action="ServletBuscarContacto"
							class="navbar-form navbar-right" role="search">
							<input id="inputBusqueda" type="text" name="parametros"
								placeholder="Ingrese Nombre o Rut" required>
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</form>
					</div>
					<div id="user">
						<form method="post" action="ServletBuscarUsuario" class="navbar-form">
							<input id="inputBusqueda" type="text" name="parametros"
								placeholder="Ingrese Nombre Usuario" required>
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</form>
					</div>
				</li>
				<li>
					<form role="form" action="ServletLogin" method="get">
						<button type="submit" class="li-btn li-btn-primary">
							<span class="glyphicon glyphicon-log-out"> </span> Log out
						</button>
					</form>
				</li>
			</ul>
			<%
				}
			%>
		</div>
	</div>
	<div class="container">
		<h3>Ingreso de Usuario</h3>
		<form role="form" action="ServletLogin" method="post">
			<div class="form-group">
				<label for="rut">Usuario:</label> 
				<input type="text" class="form-control" name="usuario">
			</div>
			<div class="form-group">
				<label for="pwd">Contraseña:</label> 
				<input type="password" class="form-control" name="password">
			</div>
			<button type="submit" class="btn btn-success">Entrar</button>
		</form>
	</div>
</body>
</html>