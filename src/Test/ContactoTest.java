package Test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import capanegocio.*;
import orm.PDSPersistentManager;

public class ContactoTest {

	@Test
	public void testCreateDataNormal() {
		String out;
		Contacto cont = new Contacto();
		cont.setRut("1122334455");
		cont.setNombre("Test");
		cont.setApellido("Test");
		cont.setMail("Test@Test.cl");
		cont.setTelefono("11223344");
		cont.setEdad(1);
		cont.setSexo("M");
		Empresa emp = new Empresa();
		emp.setId(1);
		cont.setEmpresa(emp);
		out = cont.createData(cont);
		assertTrue(out == "Ingreso Exitoso!");
	}
	@Test
	public void testCreateDataDatosFaltantes() {
		String out;
		Contacto cont = new Contacto();
		cont.setNombre("Test");
		cont.setApellido("Test");
		cont.setTelefono("11223344");
		cont.setEdad(1);
		cont.setSexo("M");
		Empresa emp = new Empresa();
		emp.setId(1);
		cont.setEmpresa(emp);
		out = cont.createData(cont);
		assertTrue(out == "ERROR Creando contacto");
	}

	@Test
	public void testDeleteDataNormal() {
		int id= 6;
		String out;
		Contacto cont = new Contacto();
		cont.setUid(id);
		out= cont.deleteData(cont);
		assertTrue(out == "Contacto Eliminado!");
	}
	@Test
	public void testDeleteDataDatoErroneo() {
		int id= 7;
		String out;
		Contacto cont = new Contacto();
		cont.setUid(id);
		out= cont.deleteData(cont);
		assertTrue(out == "ERROR Eliminando al Contacto");
	}

	@Test
	public void testListData() {
		Contacto cont = new Contacto();
		Contacto[] out= cont.listData();
		assertTrue(out[0].getUid()==4);
	}

	@Test
	public void testRetrieveAndUpdateDataNormal() {
		String out;
		Contacto cont = new Contacto();
		cont.setUid(1);
		cont.setRut("184356961");
		cont.setNombre("Luis");
		cont.setApellido("Jara");
		cont.setMail("l.jara05@ufromail.cl");
		//dato nuevo
		cont.setTelefono("88776655");
		cont.setEdad(23);
		cont.setSexo("M");
		Empresa emp = new Empresa();
		emp.setId(1);
		cont.setEmpresa(emp);
		out = cont.retrieveAndUpdateData(cont);
		assertTrue(out == "Edicion Realizada!");
	}
	
	@Test
	public void testRetrieveAndUpdateDataIDErronea() {
		String out;
		Contacto cont = new Contacto();
		cont.setUid(2);
		cont.setRut("184356961");
		cont.setNombre("Luis");
		cont.setApellido("Jara");
		cont.setMail("l.jara05@ufromail.cl");
		//dato nuevo
		cont.setTelefono("88776655");
		cont.setEdad(23);
		cont.setSexo("M");
		Empresa emp = new Empresa();
		emp.setId(1);
		cont.setEmpresa(emp);
		out = cont.retrieveAndUpdateData(cont);
		assertTrue(out == "ERROR Actualizando contacto");
	}
	@Test
	public void testRetrieveAndUpdateDataDatoErroneo() {
		String out;
		Contacto cont = new Contacto();
		cont.setUid(1);
		cont.setRut("184356961");
		cont.setNombre("Luis");
		cont.setApellido("Jara");
		//cont.setMail("l.jara05@ufromail.cl");
		//dato nuevo
		cont.setTelefono("88776655");
		cont.setEdad(23);
		cont.setSexo("M");
		Empresa emp = new Empresa();
		emp.setId(1);
		cont.setEmpresa(emp);
		out = cont.retrieveAndUpdateData(cont);
		assertTrue(out == "ERROR Actualizando contacto");
	}

	@Test
	public void testGetContactoByRut() {
		String rut= "184356961";
		Contacto out[] = new Contacto[1];
		Contacto cont = new Contacto();
		cont.setRut(rut);
		out[0]= cont.getContactoByRut(cont);
		assertTrue(out[0].getNombre().equals("Luis"));
	}

	@Test
	public void testGetContactoByNombre() {
		String nombre= "Luis";
		Contacto out[] = new Contacto[1];
		Contacto cont = new Contacto();
		cont.setNombre(nombre);
		out= cont.getContactoByNombre(cont);
		assertTrue(out[0].getRut().equals("184356961"));
	}

	@Test
	public void testGetContactoByCriteria() {
		String nombre= "Luis";
		String mail= "l.jara05@ufromail.cl";
		Contacto out[] = new Contacto[1];
		Contacto cont = new Contacto();
		cont.setNombre(nombre);
		cont.setMail(mail);
		out= cont.getContactoByCriteria(cont);
		assertTrue(out[0].getRut().equals("184356961"));
	}

}
