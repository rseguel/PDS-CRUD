package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import capanegocio.*;

public class UsuarioTest {

	@Test
	public void testCreateDataNormal() {
		String out;
		Usuario user = new Usuario();
		user.setUser("vcardenas01");
		user.setPassword("contraseña");
		out = user.createData(user);
		assertTrue(out == "Ingreso Exitoso!");
	}
	@Test
	public void testCreateDataDatosFaltantes() {
		String out;
		Usuario user = new Usuario();
		user.setUser("vcardenas01");
		out = user.createData(user);
		assertTrue(out == "ERROR Creando contacto");
	}

	@Test
	public void testDeleteDataNormal() {
		int id= 2;
		String out;
		Usuario user = new Usuario();
		user.setUid(id);
		out= user.deleteData(user);
		assertTrue(out == "Usuario Eliminado!");
	}
	@Test
	public void testDeleteDataDatoErroneo() {
		int id= 7;
		String out;
		Usuario user = new Usuario();
		user.setUid(id);
		out= user.deleteData(user);
		assertTrue(out == "ERROR. No se ha podido borrar el usuario!");
	}

	@Test
	public void testListData() {
		Usuario cont = new Usuario();
		Usuario[] out= cont.listData();
		assertTrue(out[0].getUid()==4);
	}

	@Test
	public void testRetrieveAndUpdateDataNormal() {
		String out;
		Usuario user = new Usuario();
		user.setUid(1);
		user.setUser("ljara05");
		user.setPassword("1234");
		out = user.retrieveAndUpdateData(user);
		assertTrue(out == "Edicion Realizada!");
	}
	
	@Test
	public void testRetrieveAndUpdateDataIDErronea() {
		String out;
		Usuario user = new Usuario();
		user.setUid(15);
		user.setUser("ljara05");
		user.setPassword("1234");
		out = user.retrieveAndUpdateData(user);
		assertTrue(out == "ERROR Actualizando Usuario");
	}
	@Test
	public void testRetrieveAndUpdateDataDatoErroneo() {
		String out;
		Usuario user = new Usuario();
		user.setUid(1);
		user.setUser("ljara05");
		//user.setPassword("1234");
		out = user.retrieveAndUpdateData(user);
		assertTrue(out == "ERROR Actualizando contacto");
	}

	@Test
	public void testGetUsuarioByUser() {
		String usuario= "ljara05";
		Usuario out = new Usuario();
		Usuario user = new Usuario();
		user.setUser(usuario);
		out= user.getUsuarioByUser(user);
		assertTrue(out.getPassword().equals("1234"));
	}

}
