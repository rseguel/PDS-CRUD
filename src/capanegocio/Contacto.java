package capanegocio;

import java.util.ArrayList;
import java.util.Arrays;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.Insert;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import com.google.gson.Gson;

import orm.ContactoCriteria;
import orm.PDSPersistentManager;

public class Contacto {
	private int uid;

	private String rut;

	private String nombre;

	private String apellido;

	private String mail;

	private String telefono;

	private Integer edad;

	private String sexo;

	private Empresa empresa;

	public Contacto() {

	}

	/**
	 * Contruye la clase contacto con todos los parametros.
	 * 
	 * @param rut - String - rut del contacto
	 * @param nombre - String - nombre del contacto
	 * @param apellido - String - apellido del contacto
	 * @param mail - String - correo electronico del contacto
	 * @param telefono - String - telefono del contacto
	 * @param edad - String - edad del contacto
	 * @param sexo - String - sexo del contacto
	 */
	public Contacto(String rut, String nombre, String apellido, String mail, String telefono, int edad, String sexo) {
		this.rut = rut;
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.telefono = telefono;
		this.edad = edad;
		this.sexo = sexo;
	}

	/**
	 * Crea un contacto en la base de datos
	 * @param contacto - Contacto - Contacto para ser ingresado en la base de datos
	 * @return String - Resultado del ingreso
	 * @throws PersistentException
	 */
	public String createData(Contacto contacto) {
		PersistentTransaction t = null;
		String out = "";
		try {
			t = PDSPersistentManager.instance().getSession().beginTransaction();
			orm.Contacto cont = orm.ContactoDAO.createContacto();
			String rut= contacto.getRut();
			rut.replace("K", "k");
			cont.setRut(rut);
			cont.setNombre(contacto.getNombre());
			cont.setApellido(contacto.getApellido());
			cont.setMail(contacto.getMail());
			cont.setTelefono(contacto.getTelefono());
			cont.setEdad(contacto.getEdad());
			cont.setSexo(contacto.getSexo());
			cont.setEmpresa(contacto.getEmpresa().toORMEmpresa());
			orm.ContactoDAO.save(cont);
			t.commit();
			out = "Ingreso Exitoso!";
		} catch (Exception e) {
			try {
				t.rollback();
			} catch (PersistentException e1) {
				out = "ERROR Volviendo todo atras.";
			}
			out = "ERROR Creando contacto";
		}
		return out;
	}

	/**
	 * Elimina un contacto de la base de datos
	 * @param contacto - Contacto - Contacto para eliminar en la base de datos
	 * @return String - Mensaje con el resultado de la eliminacion
	 * @throws PersistentException
	 */
	public String deleteData(Contacto c) {
		PersistentTransaction t = null;
		String out = "";
		int id = c.getUid();
		try {
			t = PDSPersistentManager.instance().getSession().beginTransaction();
			orm.Contacto contacto = orm.ContactoDAO.loadContactoByORMID(id);
			orm.ContactoDAO.delete(contacto);
			t.commit();
			out = "Contacto Eliminado!";
		} catch (Exception e) {
			try {
				t.rollback();
			} catch (PersistentException e1) {
				out = "ERROR Volviendo todo atras.";
				return out;
			}
			out = "ERROR Eliminando al Contacto";
			return out;
		}
		return out;
	}

	/**
	 * Obtiene la lista de contactos de la base de datos
	 * @return Contacto[] - arreglo de contactos
	 * @throws Exception
	 */
	public Contacto[] listData() {
		orm.Contacto[] aux;
		Contacto[] out = null;
		try {
			aux = orm.ContactoDAO.listContactoByQuery(null, null);
			out = fromORMContactoToContacto(aux);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	/**
	 * Actualiza un contacto existente con nuevos datos
	 * @param c - Contacto - contacto que sera actualizado con los nuevos parametros
	 * @return String - Resultado de la edicion
	 * @throws PersistentException,  Exception
	 */
	public String retrieveAndUpdateData(Contacto c) {
		PersistentTransaction t = null;
		String out = "";
		int id = c.getUid();
		try {
			t = PDSPersistentManager.instance().getSession().beginTransaction();
			orm.Contacto contacto = orm.ContactoDAO.loadContactoByORMID(id);
			String rut= c.getRut();
			rut.replace("K", "k");
			contacto.setRut(rut);
			contacto.setNombre(c.getNombre());
			contacto.setApellido(c.getApellido());
			contacto.setMail(c.getMail());
			contacto.setTelefono(c.getTelefono());
			contacto.setEdad(c.getEdad());
			contacto.setSexo(c.getSexo());
			Gson gson = new Gson();
			contacto.setEmpresa(c.getEmpresa().toORMEmpresa());
			orm.ContactoDAO.save(contacto);
			t.commit();
			out = "Edicion Realizada!";
		} catch (Exception e) {
			try {
				t.rollback();
			} catch (PersistentException e1) {
				out = "ERROR Volviendo todo atras.";
			}
			out = "ERROR Actualizando contacto";
		}
		return out;
	}

	/**
	 * Busca un contacto en la base de datos.
	 * @param cont - contacto con el rut para realizar la busqueda
	 * @return contacto - contacto encontrado resultante
	 * @throws Exception
	 */
	public Contacto getContactoByRut(Contacto cont) {
		orm.Contacto contacto = new orm.Contacto();
		Contacto contSalida = new Contacto();
		if (cont.getRut().trim().length() != 0 && !cont.getRut().equals(null)) {
			String rut = cont.getRut();
			rut.replace("K","k");
			try {
				contacto = orm.ContactoDAO.loadContactoByQuery("rut='" + rut + "'", null);
				if (contacto != null) {
					contSalida = fromORMtoContacto(contacto);
				}
			} catch (Exception e) {
				return contSalida;
			}
		}
		return contSalida;
	}

	/**
	 * Busca un contacto en la base de datos.
	 * @param cont - contacto con el nombre para realizar la busqueda
	 * @return contacto - contacto encontrado resultante
	 * @throws Exception
	 */
	public Contacto[] getContactoByNombre(Contacto cont) {
		orm.Contacto[] contactos = null;
		Contacto[] contsSalida = null;
		if (cont.getNombre().trim().length() != 0 && !cont.getNombre().equals(null)) {
			try {
				ContactoCriteria ccr= new ContactoCriteria();
				String nombre = cont.getNombre();
				nombre.toLowerCase();
				ccr.add(Restrictions.ilike("nombre", nombre));
				contactos = orm.ContactoDAO.listContactoByCriteria(ccr);
				contsSalida = fromORMContactoToContacto(contactos);
			} catch (Exception e) {
				return contsSalida;
			}
		}
		return contsSalida;
	}
	/**
	 * Busca un contacto en la base de datos segun varios parametros.
	 * @param cont - contacto con los parametros o almenos uno para realizar la busqueda
	 * @return contacto[] - contactos encontrado resultante
	 * @throws Exception
	 */
	public Contacto[] getContactoByCriteria(Contacto cont) {
		orm.Contacto[] contactos = new orm.Contacto[1];
		Contacto[] contsSalida = new Contacto[1];
		ContactoCriteria ccr=null;
		try {
			ccr = new ContactoCriteria();
		} catch (PersistentException e) {
			return contsSalida;
		}
		if (cont.getRut()!=(null)) {
			if(cont.getRut().trim().length() != 0){
				ccr.add(Restrictions.ilike("rut", cont.getRut().toLowerCase()));
			}	
		}
		if (cont.getNombre()!=(null)) {
			if (cont.getNombre().trim().length() != 0) {
				ccr.add(Restrictions.ilike("nombre", cont.getNombre().toLowerCase()));
			}	
		}
		if (cont.getApellido()!=(null)) {
			if(cont.getApellido().trim().length() != 0){
			ccr.add(Restrictions.ilike("apellido", cont.getApellido().toLowerCase()));
			}
		}
		if (cont.getMail()!=(null)) {	
			if (cont.getMail().trim().length() != 0) {
				ccr.add(Restrictions.ilike("mail", cont.getMail().toLowerCase()));
			}	
		}
		try {
			contactos = orm.ContactoDAO.listContactoByCriteria(ccr);
			contsSalida = fromORMContactoToContacto(contactos);
		} catch (Exception e) {
			return contsSalida;
		}
		return contsSalida;
	}
	
	/**
	 * Clace Primaria
	 */
	public void setUid(int value) {
		this.uid = value;
	}

	/**
	 * Clace Primaria
	 */
	public int getUid() {
		return uid;
	}

	public int getORMID() {
		return getUid();
	}

	/**
	 * Rut
	 */
	public void setRut(String value) {
		this.rut = value;
	}

	/**
	 * Rut
	 */
	public String getRut() {
		return rut;
	}

	/**
	 * Nombre
	 */
	public void setNombre(String value) {
		this.nombre = value;
	}

	/**
	 * Nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Apellido
	 */
	public void setApellido(String value) {
		this.apellido = value;
	}

	/**
	 * Apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * Mail
	 */
	public void setMail(String value) {
		this.mail = value;
	}

	/**
	 * Mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Telefono
	 */
	public void setTelefono(String value) {
		this.telefono = value;
	}

	/**
	 * Telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * Edad
	 */
	public void setEdad(int value) {
		setEdad(new Integer(value));
	}

	/**
	 * Edad
	 */
	public void setEdad(Integer value) {
		this.edad = value;
	}

	/**
	 * Edad
	 */
	public Integer getEdad() {
		return edad;
	}

	/**
	 * Sexo
	 */
	public void setSexo(String value) {
		this.sexo = value;
	}

	/**
	 * Sexo
	 */
	public String getSexo() {
		return sexo;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Metodo interno que transforma un capanegocio.Contacto a orm.Contacto
	 * @return orm.Contacto - contacto transformado
	 */
	private orm.Contacto toORMContacto() {
		orm.Contacto contacto = new orm.Contacto();
		contacto.setUid(this.getUid());
		contacto.setNombre(this.getNombre());
		contacto.setApellido(this.getApellido());
		contacto.setMail(this.getMail());
		contacto.setTelefono(this.getTelefono());
		contacto.setRut(this.getRut());
		contacto.setSexo(this.getSexo());
		contacto.setEdad(this.getEdad());
		orm.Empresa emp = new orm.Empresa();
		emp.setId(this.getEmpresa().getId());
		emp.setNombre(this.getEmpresa().getNombre());
		contacto.setEmpresa(emp);
		return contacto;
	}

	/**
	 * Metodo interno que transforma un orm.Contacto a capanegocio.Contacto
	 * @return capanegocio.Contacto - contacto transformado
	 */
	private Contacto fromORMtoContacto(orm.Contacto cont) {
		Contacto contacto = new Contacto();
		contacto.setUid(cont.getUid());
		contacto.setNombre(cont.getNombre());
		contacto.setApellido(cont.getApellido());
		contacto.setMail(cont.getMail());
		contacto.setTelefono(cont.getTelefono());
		contacto.setRut(cont.getRut());
		contacto.setSexo(cont.getSexo());
		contacto.setEdad(cont.getEdad());
		Empresa emp = new Empresa();
		emp.setId(cont.getEmpresa().getId());
		emp.setNombre(cont.getEmpresa().getNombre());
		contacto.setEmpresa(emp);
		return contacto;
	}

	/**
	 * Metodo interno que transforma un arreglo de orm.Contacto a un arreglo de capanegocio.Contacto
	 * @return capanegocio.Contacto[] - areglo de orn.Contacto transformado
	 */
	private Contacto[] fromORMContactoToContacto(orm.Contacto[] contactos) {
		Contacto[] conts = new Contacto[contactos.length];
		for (int i = 0; i < conts.length; i++) {
			Contacto c = new Contacto();
			c.setUid(contactos[i].getUid());
			c.setNombre(contactos[i].getNombre());
			c.setApellido(contactos[i].getApellido());
			c.setMail(contactos[i].getMail());
			c.setTelefono(contactos[i].getTelefono());
			c.setRut(contactos[i].getRut());
			c.setEdad(contactos[i].getEdad());
			c.setSexo(contactos[i].getSexo());
			Empresa emp = new Empresa();
			emp.setId(contactos[i].getEmpresa().getId());
			emp.setNombre(contactos[i].getEmpresa().getNombre());
			c.setEmpresa(emp);
			conts[i] = c;
		}
		return conts;
	}
}
