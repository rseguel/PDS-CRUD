/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package capanegocio;


public class Empresa {
	public Empresa() {
	}
	
	private int id;
	
	private String nombre;
		
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	/**
	 * Transforma un Empresa de tipo capanegocio.Empresa a uno de tipo orm.Empresa
	 * @return orm.Empresa - variable transformada al tipo orm.Empresa
	 */
	public orm.Empresa toORMEmpresa(){
		orm.Empresa out= new orm.Empresa();
		out.setId(this.id);
		out.setNombre(this.nombre);
		return out;
	}
}
