/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package capanegocio;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.PDSPersistentManager;

public class Usuario {
	public Usuario() {
	}

	private int uid;

	private String user;

	private String password;

	/**
	 * Constructor de la clase Usuario
	 * @param user - String - usuario de la clase
	 * @param password - String - contrase�a del usuario
	 */
	public Usuario(String user, String password) {
		this.user = user;
		this.password = password;

	}

	/**
	 * Crea un usuario en la base de datos
	 * @param usuario - Usuario - Usuario para ser ingresado en la base de datos
	 * @return String - Resultado del ingreso
	 * @throws PersistentException
	 */
	public String createData(Usuario usuario) {
		PersistentTransaction t = null;
		String out = "";
		try {
			t = PDSPersistentManager.instance().getSession().beginTransaction();
			orm.Usuario user = orm.UsuarioDAO.createUsuario();
			user.setUser(usuario.getUser());
			user.setPassword(usuario.getPassword());
			orm.UsuarioDAO.save(user);
			t.commit();
			out = "Ingreso Exitoso!";
		} catch (Exception e) {
			try {
				t.rollback();
			} catch (PersistentException e1) {
				out = "ERROR Volviendo todo atras.";
			}
			out = "ERROR. No se ha podido crear el usuario!";
		}
		return out;
	}

	/**
	 * Elimina un usuario de la base de datos
	 * @param usuario - Usuario - Usuario para eliminar en la base de datos
	 * @return String - Mensaje con el resultado de la eliminacion
	 * @throws PersistentException
	 */
	public String deleteData(Usuario u) {
		PersistentTransaction t = null;
		String out = "";
		int id = u.getUid();
		try {
			t = PDSPersistentManager.instance().getSession().beginTransaction();
			orm.Usuario usuario = orm.UsuarioDAO.loadUsuarioByORMID(id);
			orm.UsuarioDAO.delete(usuario);
			t.commit();
			out = "Usuario Eliminado!";
		} catch (Exception e) {
			try {
				t.rollback();
			} catch (PersistentException e1) {
				out = "ERROR Volviendo todo atras.";
			}
			out = "ERROR. No se ha podido borrar el usuario!";
		}
		return out;
	}

	/**
	 * Obtiene la lista de usuarios de la base de datos
	 * @return GSON - Salida con un arreglo de tipo Usuario en forma de GSON
	 * @throws Exception
	 */
	public Usuario[] listData() {
		orm.Usuario[] aux;
		Usuario[] usuariosSalida= null;
		try {
			aux = orm.UsuarioDAO.listUsuarioByQuery(null, null);
			usuariosSalida= fromORMArrayToUsuarioArray(aux);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuariosSalida;
	}

	/**
	 * Busca un usuario en la base de datos.
	 * @param user - parametro de busqueda
	 * @return salida - GSON - usuario encontrado resultante o mensaje en forma de GSON
	 * @throws Exception
	 */
	public Usuario getUsuarioByUser(Usuario user){
		orm.Usuario usuario = null;
		Usuario usuarioSalida = null;
		if(user.getUser().trim().length()!=0 && !user.getUser().equals(null)){
			try {
				usuario= orm.UsuarioDAO.loadUsuarioByQuery("user='"+user.getUser()+"'", null);
				usuarioSalida = fromORMtoUsuario(usuario);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			usuarioSalida.setUser("No existe el usuario");
		}
		return usuarioSalida;
	}

	/**
	 * Actualiza un Usuario existente con nuevos datos
	 * @param u - Usuario - Usuario que sera actualizado con los nuevos parametros
	 * @return String - Resultado de la edicion
	 * @throws PersistentException, Exception
	 */
	public String retrieveAndUpdateData(Usuario u) {
		PersistentTransaction t = null;
		String out = "";
		try {
			int id = u.getUid();
			t = PDSPersistentManager.instance().getSession().beginTransaction();
			orm.Usuario usuario = orm.UsuarioDAO.loadUsuarioByORMID(id);
			// Update the properties of the persistent object
			usuario.setUser(u.getUser());
			usuario.setPassword(u.getPassword());
			orm.UsuarioDAO.save(usuario);
			t.commit();
			out = "Edicion Realizada!";
		} catch (Exception e) {
			try {
				t.rollback();
			} catch (PersistentException e1) {
				out = "ERROR Volviendo todo atras.";
			}
			out = "ERROR Actualizando Usuario";
		}
		return out;
	}

	public void setUid(int value) {
		this.uid = value;
	}

	public int getUid() {
		return uid;
	}

	public int getORMID() {
		return getUid();
	}

	public void setUser(String value) {
		this.user = value;
	}

	public String getUser() {
		return user;
	}

	public void setPassword(String value) {
		this.password = value;
	}

	public String getPassword() {
		return password;
	}

	public String toString() {
		return String.valueOf(getUid());
	}
	/**
	 * Metodo interno que transforma un capanegocio.Usuario a orm.Usuario
	 * @return orm.Usuario - Usuario transformado al tipo orm.Usuario
	 */
	private orm.Usuario toORMUsuario() {
		orm.Usuario usuario = new orm.Usuario();
		usuario.setUid(this.getUid());
		usuario.setUser(this.getUser());
		usuario.setPassword(this.getPassword());
		return usuario;
	}
	/**
	 * Metodo interno que transforma un orm.Usuario a capanegocio.Usuario
	 * @return capanegocio.Usuario - Usuario transformado
	 */
	private Usuario fromORMtoUsuario(orm.Usuario user) {
		Usuario usuario = new Usuario();
		usuario.setUid(user.getUid());
		usuario.setUser(user.getUser());
		usuario.setPassword(user.getPassword());
		return usuario;
	}
	/**
	 * Metodo interno que transforma un arreglo de  orm.Usuario a un arreglo de capanegocio.Usuario
	 * @return capanegocio.Usuario[] - areglo de orn.Usuario transformado
	 */
	private Usuario[] fromORMArrayToUsuarioArray(orm.Usuario[] usuarios){
		Usuario[] users= new Usuario[usuarios.length];
		for (int i = 0; i < users.length; i++) {
			Usuario u= new Usuario();
			u.setUid(usuarios[i].getUid());
			u.setUser(usuarios[i].getUser());
			u.setPassword(usuarios[i].getPassword());
			users[i]= u;
		}
		return users;
	}
}
