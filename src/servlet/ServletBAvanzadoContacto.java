package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import capanegocio.Contacto;

/**
 * Servlet implementation class ServletBAvanzadoContacto
 */
@WebServlet("/ServletBAvanzadoContacto")
public class ServletBAvanzadoContacto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletBAvanzadoContacto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String msg = "";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			String rut = "";
			String nombre = "";
			String apellido = "";
			String mail = "";
			Contacto[] contactos = new Contacto[1];
			Contacto cont = new Contacto();
			try {
				rut = request.getParameter("rut");
				nombre = request.getParameter("nombre");
				apellido = request.getParameter("apellido");
				mail = request.getParameter("mail");
				if((rut.equals(null) || rut.trim().length() == 0) && 
						(nombre.equals(null) || nombre.trim().length() == 0) &&
						(apellido.equals(null) || apellido.trim().length() == 0) &&
						(mail.equals(null) || mail.trim().length() == 0)){
					msg = "Error: Debe ingresar aunque sea un parametro de busqueda";
				}
				if (rut.trim().length() != 0) {
					cont.setRut(rut);
				}else{
					cont.setRut("");
				}
				if (nombre.trim().length() != 0) {
					cont.setNombre(nombre);
				}else{
					cont.setNombre("");
				}
				if (apellido.trim().length() != 0) {
					cont.setApellido(apellido);
				}else{
					cont.setApellido("");
				}
				if (mail.trim().length() != 0) {
					cont.setMail(mail);
				}else{
					cont.setMail("");
				}
				try {
					contactos= cont.getContactoByCriteria(cont);
					if(contactos.length!=0){
						request.setAttribute("resultado", contactos);
						request.getRequestDispatcher("/resultadosContacto.jsp").forward(request, response);
					}else{
						msg = "Sin Resultados";
					}
				} catch (Exception e) {
					e.printStackTrace();
					msg = "Error: No se pudo buscar contactos";
				}
			} catch (Exception e) {
				msg = "Error: No se pudo cargar datos del formulario";
			}
		}else{
			msg="Sesion no iniciada!";
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
		
	}

}
