package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import capanegocio.Contacto;
import capanegocio.Usuario;

/**
 * Servlet implementation class ServletBuscarContacto
 */
@WebServlet("/ServletBuscarContacto")
public class ServletBuscarContacto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletBuscarContacto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parametros="";
		String msg = "";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			try{
				parametros=request.getParameter("parametros");
				if(parametros.trim().length()!=0){
					try{
						String parametros2=parametros;	
						parametros= parametros.trim();
						if(Character.isDigit(parametros.charAt(0))){
							try {
								Contacto cont = new Contacto();
								cont.setRut(parametros);
								Contacto[] aux = new Contacto[1];
								aux[0]= cont.getContactoByRut(cont);
								if(aux[0].getUid()==0){
									msg="Sin Resultados";
								}else{
									request.setAttribute("resultado", aux);
									request.getRequestDispatcher("/resultadosContacto.jsp").forward(request, response);
								}
							} catch (Exception e) {
								msg="Error: No se pudo buscar el contacto1";
							}
						}else{
							int largo= parametros2.length()-1;
							boolean out=false;
							while(out==false) {
								if(!Character.isLetter(parametros2.charAt(largo))){
									parametros2=parametros2.substring(0,largo-1);
									largo--;
								}else{
									out=true;
								}
							}
							try {
								Contacto cont = new Contacto();
								cont.setNombre(parametros2);
								Contacto[] aux = cont.getContactoByNombre(cont);
								if(aux.length==0){
									msg="Sin Resultados";
								}else{
									request.setAttribute("resultado", aux);
									request.getRequestDispatcher("/resultadosContacto.jsp").forward(request, response);
								}
							} catch (Exception e) {
								msg="Error: No se pudo buscar el contacto";
							}
						}
					}catch(Exception e){
						msg="Error: No deje el espacio en blanco";
					}
				}else{
					msg="Error: No deje el espacio en blanco";
				}
			}catch(Exception e){
				msg="Error: No se pudo cargar parametros de busqueda";
			}
		}else{
			msg="Sesion no iniciada!";
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

}
