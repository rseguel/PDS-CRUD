package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import capanegocio.Contacto;
import capanegocio.Empresa;
import capanegocio.Usuario;

/**
 * Servlet implementation class ServletBuscarUsuario
 */
@WebServlet("/ServletBuscarUsuario")
public class ServletBuscarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletBuscarUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String parametros = "";
		String msg = "";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			try {
				parametros = request.getParameter("parametros");
				if (parametros.trim().length() != 0) {
					try {
						parametros=parametros.trim();
						try {
							Usuario user = new Usuario();
							user.setUser(parametros);
							Usuario[] aux = new Usuario[1];
							aux[0] = user.getUsuarioByUser(user);
							if (aux[0].getUid() == 0) {
								msg = "Error: Sin Resultados";
							} else {
								request.setAttribute("resultado", aux);
								request.getRequestDispatcher("/resultadosUsuario.jsp").forward(request, response);
							}
						} catch (Exception e) {
							msg = "Error: No se pudo buscar el contacto";
						}
					} catch (Exception e) {
						msg = "Error: No deje el espacio en blanco";
					}
				} else {
					msg = "Error: No deje el espacio en blanco";
				}
			} catch (Exception e) {
				msg = "Error: No se pudo cargar parametros de busqueda";
			}
		}else{
			msg="Sesion no iniciada!";
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
		
	}
}
