package servlet;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import capanegocio.Usuario;
import webservice.UsuarioWS;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet("/ServletLogin")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String msg = "";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			sesion.invalidate();
			msg = "Sesion Cerrada!";
		}else{
			msg="Error: No hay una sesion iniciada";
		}	
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		String user, pass, msg = "";
		Usuario usuario = new Usuario();
		Usuario userCN = new Usuario();
		try{
			user = request.getParameter("usuario");
			pass = request.getParameter("password");
			userCN.setUser(user);
			try {
				usuario = userCN.getUsuarioByUser(userCN);
			} catch (Exception e) {
				msg = "Error: No se pudo encontrar el Usuario!";
				request.setAttribute("msg", msg);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
			if (usuario.getUser().trim().length() != 0 && !usuario.getUser().equals(null)
					&& usuario.getPassword().trim().length() != 0 && !usuario.getPassword().equals(null)) {
				if(usuario.getPassword().trim().equals(pass.trim())){
					if (sesion.getAttribute("usuario") == null) {
						sesion.setAttribute("usuario", usuario);
						msg = "Login Exitoso!";
					} else {
						msg = "Error: sesion ya iniciada!";
					}
				}else{
					msg= "Error: Contraseña Incorrecta!";
				}
			} else {
				msg = "Error: Datos incorrectos!";
			}
		}catch(Exception e){
			msg ="Error: No se pudo leer informacion del formulario!";
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}	
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
}
