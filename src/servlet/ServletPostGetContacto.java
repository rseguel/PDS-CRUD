package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import webservice.*;
import capanegocio.*;

/**
 * Servlet implementation class ServletTaller
 */
@WebServlet("/ServletPostGetContacto")
public class ServletPostGetContacto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletPostGetContacto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		String msg="";
		if(sesion.getAttribute("usuario")!=null){
			Contacto contactoCN = new Contacto();
			Contacto contactos[] = contactoCN.listData();
			request.setAttribute("resultado", contactos);
			request.getRequestDispatcher("/resultadosContacto.jsp").forward(request, response);
		}else{
			msg="Sesion no iniciada!";
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String msg = "Error: No se pudo crear el contacto!";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			Contacto contactoCN = new Contacto();
			String nombre = "";
			String apellido = "";
			String mail = "";
			String telefono = "";
			String rut = "";
			int edad = 0;
			String sexo = "";
			try{
				nombre = request.getParameter("nombre");
				apellido = request.getParameter("apellido");
				mail = request.getParameter("mail");
				telefono = request.getParameter("telefono");
				rut = request.getParameter("rut");
				edad = Integer.parseInt(request.getParameter("edad"));
				sexo = request.getParameter("sexo");
				if ((nombre == null) || (nombre.trim().length() == 0) || (apellido == null) || (apellido.trim().length() == 0)
						|| (mail == null) || (mail.trim().length() == 0) || (telefono == null)
						|| (telefono.trim().length() == 0) || (rut == null) || (rut.trim().length() == 0) || (edad == 0)
						|| (sexo == null) || (sexo.trim().length() == 0)) {
					msg = "Error: Por favor no deje campos vacios";
				} else {
					Contacto contacto = new Contacto(rut, nombre, apellido, mail, telefono, edad, sexo);
					Empresa empresa = new Empresa();
					empresa.setId(1);
					contacto.setEmpresa(empresa);
					try {
						msg = contactoCN.createData(contacto);
					} catch (Exception e) {
						msg = "Error: No se pudo crear el contacto!";
					}
				}
			}catch(Exception e){
				msg ="Error: No se pudo leer informacion del formulario!";
			}
		}else{
			msg="Sesion no iniciada!";
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

}
