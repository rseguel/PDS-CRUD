package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import capanegocio.Contacto;
import capanegocio.Empresa;
import capanegocio.Usuario;

/**
 * Servlet implementation class ServletPostGetUser
 */
@WebServlet("/ServletPostGetUsuario")
public class ServletPostGetUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletPostGetUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		String msg = "";
		if (sesion.getAttribute("usuario") != null) {
			Usuario usuarioCN = new Usuario();
			Usuario usuarios[] = usuarioCN.listData();
			request.setAttribute("resultado", usuarios);
			request.getRequestDispatcher("/resultadosUsuario.jsp").forward(request, response);
		} else {
			msg = "Sesion no iniciada!";
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String msg = "";
		Usuario usuarioCN = new Usuario();
		String usuario = "";
		String password = "";
		try {
			usuario = request.getParameter("usuario");
			password = request.getParameter("password");
			if ((usuario == null) || (usuario.trim().length() == 0) || (password == null)
					|| (password.trim().length() == 0)) {
				msg = "Error: Por favor no deje campos vacios";
			} else {
				Usuario user = new Usuario(usuario, password);
				try {
					usuarioCN.createData(user);
					msg = "Usuario creado con exito!";
				} catch (Exception e) {
					msg = "Error: No se pudo crear el Usuario!";
				}

			}
		} catch (Exception e) {
			msg = "Error: No se pudo leer informacion del formulario!";
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
}
