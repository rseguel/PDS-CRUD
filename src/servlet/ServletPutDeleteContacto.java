package servlet;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import capanegocio.*;
import webservice.*;
/**
 * Servlet implementation class ServletPutDelete
 */
@WebServlet("/ServletPutDeleteContacto")
public class ServletPutDeleteContacto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletPutDeleteContacto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		String msg="";
		if(sesion.getAttribute("usuario")!=null){
			String action= "";
			try{
				action = request.getParameter("action");
				if (action.equals("Eliminar")) {
					doDelete(request, response);
				} else {
					doPut(request, response);
				}
			}catch(Exception e){
				msg= "ERROR: no se pudo cargar accion!";
			}
		}else{
			msg="Sesion no iniciada!";			
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String msg="Error: No se pudo crear el contacto!";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			Contacto contactoCN= new Contacto();
			int id = 0;
			String nombre = "";
			String apellido = "";
			String mail = "";
			String telefono = "";
			String rut = "";
			int edad = 0;
			String sexo = ""; 
			try{
				id = Integer.parseInt(request.getParameter("id"));
				nombre = request.getParameter("nombre");
				apellido = request.getParameter("apellido");
				mail = request.getParameter("mail");
				telefono = request.getParameter("telefono");
				rut = request.getParameter("rut");
				edad = Integer.parseInt(request.getParameter("edad"));
				sexo = request.getParameter("sexo"); 
				if ((id == 0) ||
						(nombre == null) || (nombre.trim().length() == 0) 
						|| (apellido == null) || (apellido.trim().length() == 0)
						|| (mail == null) || (mail.trim().length() == 0) 
						|| (telefono == null) || (telefono.trim().length() == 0)
						|| (rut == null) || (rut.trim().length() == 0)
						|| (edad == 0)
						|| (sexo == null) || (sexo.trim().length() == 0)) 
				{
					msg="Error: Por favor no deje campos vacios";
				} else {
					Contacto contacto = new Contacto(rut,nombre,apellido,mail,telefono,edad,sexo);
					contacto.setUid(id);
					try {
						msg= contactoCN.retrieveAndUpdateData(contacto);
					} catch (Exception e) {
						msg= "Error: no se pudo actualizar el contacto";
					}
				}
			}catch(Exception e){
				msg= "Error: no se pudo leer informacion del formulario!";
			}
		}else{
			msg="Sesion no iniciada!";			
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String msg="Error: no se pudo eliminar el Contacto!";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			Contacto contactoCN= new Contacto();
			String id = "";
			try{
				id = request.getParameter("id");
				Contacto aux= new Contacto();
				aux.setUid(Integer.parseInt(id));
				if((!id.equals(null) && id.trim().length()!=0)){
					try {
						msg=contactoCN.deleteData(aux);
					}catch (Exception e) {
						msg= "Error: No se pudo eliminar el contacto!";
					}
				}else{
					msg="Error: No existe el Contacto!";
				}
			}catch(Exception e){
				msg= "Error: ID no pudo ser encontrada!";
			}
		}else{
			msg="Sesion no iniciada!";			
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);		
	}
}
