package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import capanegocio.Contacto;
import capanegocio.Usuario;

/**
 * Servlet implementation class ServletPutDeleteUsuario
 */
@WebServlet("/ServletPutDeleteUsuario")
public class ServletPutDeleteUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPutDeleteUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		String msg="";
		if(sesion.getAttribute("usuario")!=null){
			String action= "";
			try{
				action = request.getParameter("action");
				if (action.equals("Eliminar")) {
					doDelete(request, response);
				} else {
					doPut(request, response);
				}
			}catch(Exception e){
				msg= "Error: no se pudo cargar accion!";
			}
		}else{
			msg="Sesion no iniciada!";			
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg="Error: No se pudo editar el usuario!";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			Usuario usuarioCN= new Usuario();
			int id = 0;
			String user = "";
			String password = "";
			try{
				id = Integer.parseInt(request.getParameter("id"));
				user = request.getParameter("usuario");
				password = request.getParameter("password");
				if ((id == 0) ||
						(user == null) || (user.trim().length() == 0) ||
						(password == null) || (password.trim().length() == 0) ) 
				{
					msg="Error: Por favor no deje campos vacios";
				} else {
					Usuario usuario = new Usuario();
					usuario.setUid(id);
					try {
						msg= usuarioCN.retrieveAndUpdateData(usuario);
					} catch (Exception e) {
						msg= "Error: No se pudo actualizar el usuario";
					}
				}
			}catch(Exception e){
				msg= "Error: No se pudo leer informacion del formulario!";
			}
		}else{
			msg="Sesion no iniciada!";			
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);	
		
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg="Error: no se pudo eliminar el Usuario!";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			Usuario usuarioCN= new Usuario();
			String id = "";
			try{
				id = request.getParameter("id");
				Usuario aux= new Usuario();
				aux.setUid(Integer.parseInt(id));
				if((!id.equals(null) && id.trim().length()!=0)){
					try {
						msg=usuarioCN.deleteData(aux);
					}catch (Exception e) {
						msg= "Error: No se pudo eliminar el Usuario!";
					}
				}else{
					msg="Error: No existe el Usuario!";
				}
			}catch(Exception e){
				msg= "Error: ID no pudo ser encontrada!";
			}
		}else{
			msg="Sesion no iniciada!";			
		}
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/index.jsp").forward(request, response);	
	}

}
