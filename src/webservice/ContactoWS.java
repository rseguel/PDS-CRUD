package webservice;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;

import com.google.gson.Gson;

import capanegocio.Contacto;

public class ContactoWS {
	/**
	 * Metodo que transforma el parametro ingresado, envia a la capa superior para su ingreso a la base de datos
	 * y luego recibe y devulve la respuesta
	 * @param c - GSON - contacto en forma de GSON para su ingreso en la base de datos
	 * @return String - mensaje resultante del ingreso
	 * @throws SoapException
	 */
	public String createData(String c) throws SoapException {
		Contacto contacto = new Contacto();
		Gson gs = new Gson();
		contacto = gs.fromJson(c, Contacto.class);
		if (c.equals(null) || c.trim().length()==0) {
			Throwable t = new IllegalArgumentException("Contacto Vacio!");
			throw new SoapException("Error Contacto Vacio!", t);
		}
		String out = "";
		Contacto cont = new Contacto();
		out = cont.createData(contacto);
		return out;
	}
	/**
	 * Devuelve la lista de usuarios en formato GSON
	 * @return GSON - lista de contactos en GSON
	 */
	public String listData() {
		Contacto cont = new Contacto();
		String salida = "";
		Contacto[] contactos = cont.listData();
		Gson gson= new Gson();
		salida = gson.toJson(contactos);
		return salida;
	}
	/**
	 * Metodo que transforma el parametro ingresado, envia a la capa superior para su edicion en la base de datos
	 * y luego recibe y devulve la respuesta 
	 * @param c - GSON - contacto en forma de GSON para su edicion en la base de datos
	 * @return String - mensaje resultante de la edicion
	 * @throws SoapException
	 */
	public String retrieveAndUpdateData(String c) throws SoapException {
		String salida = "";
		Gson gs = new Gson();
		Contacto contacto = gs.fromJson(c, Contacto.class);
		if (c.trim().length()==0 || c.equals(null)) {
			Throwable t = new IllegalArgumentException("Campos Vacios!");
			throw new SoapException("Error Campos Vacios!", t);
		} else {	
			Contacto cont = new Contacto();
			salida = cont.retrieveAndUpdateData(contacto);
		}
		return salida;
	}
	/**
	 * Recibe una id y elimina el contacto con dicha ID
	 * @param id - String - id del contacto a eliminar
	 * @return String - mensaje resultante de la eliminacion
	 * @throws SoapException
	 */
	public String deleteData(String id) throws SoapException {
		String salida = "";
		if (!id.equals(null) && !id.equals("")) {
			char uid[] = id.toCharArray();
			for (int i = 0; i < uid.length; i++) {
				boolean result = Character.isDigit(uid[i]);
				if (result != true) {
					Throwable t = new IllegalArgumentException("ID no es un numero!");
					throw new SoapException("Error ID no es un numero!", t);
				}
			}
			Contacto cont = new Contacto();
			cont.setUid(Integer.parseInt(id));
			salida = cont.deleteData(cont);

		} else {
			Throwable t = new IllegalArgumentException("ID Vacia!");
			throw new SoapException("Error ID Vacia!", t);
		}
		return salida;
	}
	/**
	 * Busca un contacto por el rut
	 * @param cgs - GSON - contacto con los parametros de busqueda
	 * @return GSON - contacto resultante de la busqueda
	 * @throws SoapException
	 */
	public String getContactoByRut(String cgs) throws SoapException {
		Contacto cont= new Contacto();
		if (cgs.trim().length()==0 || cgs.equals(null)) {
			Throwable t = new IllegalArgumentException("Contacto Vacio!");
			throw new SoapException("Error Contacto Vacio!", t);
		}else{
			Gson gs = new Gson();
			cont= gs.fromJson(cgs, Contacto.class);
		}
		String salida = "";
		Contacto contacto= cont.getContactoByRut(cont);
		Gson gson = new Gson();
		salida = gson.toJson(contacto);
		return salida;
	}
	/**
	 * Metodo que buscar un contacto por su nombre
	 * @param cgs - GSON - contacto con el parametro de busqueda
	 * @return GSON - contactos resultante de la busqueda
	 * @throws SoapException
	 */
	public String getContactoByNombre(String cgs) throws SoapException {
		Contacto cont= new Contacto();
		if (cgs.trim().length()==0 || cgs.equals(null)) {
			Throwable t = new IllegalArgumentException("Nombre Vacio!");
			throw new SoapException("Error Nombre Vacio!", t);
		}else{
			Gson gs = new Gson();
			cont= gs.fromJson(cgs, Contacto.class);
		}
		String salida = "";
		Contacto[] contacto= cont.getContactoByNombre(cont);
		Gson gson = new Gson();
		salida = gson.toJson(contacto);
		return salida;
	}
	/**
	 * Metodo que buscar un contacto por uno o varios criterios de busqueda
	 * @param cgs - GSON - contacto con el o los parametros de busqueda
	 * @return GSON - contactos resultante de la busqueda
	 * @throws SoapException
	 */
	public String getContactoByCriteria(String cgs) throws SoapException {
		Contacto cont= new Contacto();
		if (cgs.trim().length()==0 || cgs.equals(null)) {
			Throwable t = new IllegalArgumentException("Contacto Vacio!");
			throw new SoapException("Error Contacto Vacio!", t);
		}else{
			Gson gs = new Gson();
			cont= gs.fromJson(cgs, Contacto.class);
		}
		String salida = "";
		Contacto[] contacto= cont.getContactoByCriteria(cont);
		Gson gson = new Gson();
		salida = gson.toJson(contacto);
		return salida;
	}
}
