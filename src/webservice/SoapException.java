package webservice;

public class SoapException extends Exception {	
	public SoapException(String mensaje, Throwable causa) {
		super(mensaje, causa);
	}
}
