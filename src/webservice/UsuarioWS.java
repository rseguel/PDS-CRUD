package webservice;

import com.google.gson.Gson;

import capanegocio.Contacto;
import capanegocio.Usuario;

public class UsuarioWS {
	/**
	 * Metodo que transforma el parametro ingresado, envia a la capa superior para su ingreso a la base de datos
	 * y luego recibe y devulve la respuesta
	 * @param u - GSON - Usuario en forma de GSON para su ingreso en la base de datos
	 * @return String - mensaje resultante del ingreso
	 * @throws SoapException
	 */
	public String createData(String u) throws SoapException{
		Usuario usuario = new Usuario();
		Gson gs = new Gson();
		usuario = gs.fromJson(u, Usuario.class);
		if(u.equals(null)||u.equals("")){
			Throwable t = new IllegalArgumentException("Usuario Vacio!");
	        throw new SoapException("Error Usuario Vacio!", t);
		}
		String out = "";
		Usuario user = new Usuario();
		out = user.createData(usuario);
		return out;
	}
	/**
	 * Devuelve la lista de usuarios en formato GSON
	 * @return GSON - lista de usuario en GSON
	 */
	public String listData(){
		Usuario user = new Usuario();
		String salida= "";
		Usuario[] usuarios = user.listData();
		Gson gson= new Gson();
		salida = gson.toJson(usuarios);
		return salida;
	}

	/**
	 * Metodo que transforma el parametro ingresado, envia a la capa superior para su edicion en la base de datos
	 * y luego recibe y devulve la respuesta 
	 * @param u - GSON - usuario en forma de GSON para su edicion en la base de datos
	 * @return String - mensaje resultante de la edicion
	 * @throws SoapException
	 */
	public String retrieveAndUpdateData(String u) throws SoapException{
		String salida = "";
		Gson gs = new Gson();
		Usuario usuario= gs.fromJson(u, Usuario.class);
		if(u.trim().length()!=0 && !u.equals(null)){
			Usuario user = new Usuario();
			salida=user.retrieveAndUpdateData(usuario);		
		}else{
			Throwable t = new IllegalArgumentException("Campos Vacios!");
	        throw new SoapException("Error Campos Vacios!", t);
		}	
		return salida;
	}
	/**
	 * Recibe una id y elimina el usuario con dicha ID
	 * @param id - String - id del usuario a eliminar
	 * @return String - mensaje resultante de la eliminacion
	 * @throws SoapException
	 */
	public String deleteData(String id) throws SoapException{	
		String salida = "";
		if(!id.equals(null) && !id.equals("")){
			char uid[]= id.toCharArray();
			for (int i = 0; i < uid.length; i++) {
				boolean result = Character.isDigit(uid[i]);
				if(result!=true){
					Throwable t = new IllegalArgumentException("ID no es un numero!");
			        throw new SoapException("Error ID no es un numero!", t);
				}
			}
			Usuario user= new Usuario();
			user.setUid(Integer.parseInt(id));
			salida=user.deleteData(user);
			
		}else{
			Throwable t = new IllegalArgumentException("ID Vacia!");
	        throw new SoapException("Error ID Vacia!", t);
		}
		return salida;
	}
	/**
	 * Busca un Usuario por el nombre de usuario
	 * @param user - GSON - Usuario con los parametros de busqueda
	 * @return GSON - Usuario resultante de la busqueda
	 * @throws SoapException
	 */
	public String getUsuarioByUser(String user) throws SoapException{
		if(user.equals(null)||user.equals("")){
			Throwable t = new IllegalArgumentException("Rut Vacio!");
	        throw new SoapException("Error Rut Vacio!", t);
		}
		Usuario usuario = new Usuario();
		usuario.setUser(user);
		String salida= "";
		Usuario contacto= usuario.getUsuarioByUser(usuario);
		Gson gson = new Gson();
		salida = gson.toJson(contacto);
		return salida;
	}
	
}
